# Personnal website

# Author

Hamza Mounir    


## Install 

  **Clone the repository**    
      
  ``git clone https://github.com/hamzaPixl/hamza.io.git``

  **Install differents packages**   
      
  ``npm i``

## Start 

  **Start in local**    
      
  ``npm start``

## Desktop 

  **Build app**    
      
  ``npm run build``
 
