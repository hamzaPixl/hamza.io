import React, { Component } from 'react';
import styled from 'styled-components';

import repos from '../infrastructure/repos';

const Container = styled.div`
  padding: 10%;
  font-size: 30px;
  color: ${props => props.theme.textColor};
  font-family: ${props => props.theme.fontFamily}, sans-serif;
  line-height: ${props => props.theme.lineHeight}rem;
  letter-spacing: ${props => props.theme.letterSpacing}rem;
`;

const Title = styled.div`
  color: ${props => props.theme.secondaryColor};
  padding-bottom: 5%;
`;

const RepoContainer = styled.div`
  border: 4px solid ${props => props.theme.textColor};
  border-radius: 2px;
  padding: 10px;
  margin-bottom: 60px;
  font-size: 20px;
  transition: transform .2s;
  &:hover {
    transform: scale(1.1);
  }
`;

const TitleContainer = styled.div`
  display: inline-flex;
  width: 100%;
  justify-content: space-between;
`;

const TitleRepo = styled.a`
  color: ${props => props.theme.secondaryColor};
  text-decoration:none;
  > span {
    color: ${props => props.theme.textColor};
    opacity: 0.5;
  }
`;

const Description = styled.div`
  opacity: 0.5;
`;

const InformationContainer = styled.div`
  display: inline-flex;
`;

const AdditionnalInformationContainer = styled.div`
  display: inline-flex;
  width: 100%;
  justify-content: space-between;
`;

const Information = styled.div`
  &.head {
    padding-left: 20px;
  }
`;

const Icon = styled.i`
  color: ${props => props.theme.secondaryColor};
`;

class Projects extends Component {
  constructor(props) {
    super(props);
    this.username = 'hamzaPixl';
    this.repositories = ['crypto-folio', 'hamza.io', 'intnewscr', 'food-hall-back'];
    this.state = {
      projects: [],
    };
    this.getRepositories = this.getRepositories.bind(this);
    this.getRepositories();
  }

  getRepositories() {
    Promise.all(this.repositories.map(r => repos(r, this.username)))
      .then(r => this.setState({ projects: r }));
  }

  render() {
    return (
      <Container>
        <Title>Projects</Title>
        {this.state.projects.map(p => (
          <RepoContainer key={p.repository}>
            <TitleContainer>
              <TitleRepo href={`https://github.com/${p.username}/${p.repository}`}>
                <span>{p.username} / </span>{p.repository}
              </TitleRepo>
              <InformationContainer>
                <Information className="head">
                  <Icon className="fas fa-eye" /> {p.whatchers} Watch
                </Information>
                <Information className="head">
                  <Icon className="far fa-star" /> {p.stars} Stars
                </Information>
                <Information className="head">
                  <Icon className="fas fa-share-alt" /> {p.forks} Fork
                </Information>
              </InformationContainer>
            </TitleContainer>
            <Description>{p.description}</Description>
            <AdditionnalInformationContainer>
              <Information>
                <Icon className="fas fa-code-branch" /> {p.branches} Branch
              </Information>
              <Information>
                <Icon className="fas fa-code" /> {p.commits} Commits
              </Information>
              <Information>
                <Icon className="fas fa-users" /> {p.contributors} Contributor
              </Information>
            </AdditionnalInformationContainer>
          </RepoContainer>
        ))}
      </Container>
    );
  }
}

export default Projects;
