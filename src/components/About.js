import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
  padding: 10%;
  font-size: 30px;
  color: ${props => props.theme.textColor};
  font-family: ${props => props.theme.fontFamily}, sans-serif;
  line-height: ${props => props.theme.lineHeight}rem;
  letter-spacing: ${props => props.theme.letterSpacing}rem;
`;

const Title = styled.div`
  color: ${props => props.theme.secondaryColor};
`;

const Description = styled.div`
  padding: 2% 0 2% 0;
  font-size: 20px;
`;

const InfoContainer = styled.ul`
  margin: 0;
  padding: 0;
  width: 100%;
`;

const Item = styled.li`
  display: block;
  width: 50%;
  float: left;
  font-size: 15px;
  > span {
    color: ${props => props.theme.secondaryColor};
  }
`;

const items = [
  {
    label: 'Name',
    value: 'Hamza Mounir',
  }, {
    label: 'Birthdate',
    value: '23/02/1996',
  }, {
    label: 'Nationality',
    value: 'Belgium',
  }, {
    label: 'Location',
    value: 'Brussels',
  }, {
    label: 'Email',
    value: 'hmounir.work@gmail.com',
  }, {
    label: 'Phone',
    value: '0488 20 35 67',
  }, {
    label: 'Work Status',
    value: 'Fullstack Devlopper',
  }, {
    label: 'Web',
    value: 'hamza.io',
  },
];

function About() {
  return (
    <Container>
      <Title>About Me</Title>
      <Description>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis in urna erat.
        Mauris ut mi vel arcu tristique commodo sed vel quam.
        Morbi eros mi, mattis sit amet fermentum ut,luctus sed nunc.
        Curabitur dolor orci, tristique ac rutrum non, suscipit at purus.
        Etiam molestie metus non leo cursus, sed tempus velit commodo.
        Nulla pharetra pellentesque ligula, sit amet dictum turpis pharetra vel.
        Aliquam rutrum dolor ac tempus suscipit. Sed fringilla leo at felis aliquet.
      </Description>
      <InfoContainer>
        {items.map(i => (
          <Item key={i.label}><span>{i.label} : </span>{i.value}</Item>
          ))}
      </InfoContainer>
    </Container>
  );
}

export default About;
