/* eslint no-unused-vars: 0 */
import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import Typed from 'typed.js';

const TypedText = styled.div`
  padding-top: 2%;
  transition: all 2s ease;
  height: 30px;
  color: ${props => props.theme.secondaryColor};
  > span {
    color: ${props => props.theme.textColor};
  }
`;

class Typer extends Component {
  componentDidMount() {
    const options = {
      strings: this.props.messages,
      typeSpeed: 40,
      loop: true,
      startDelay: 500,
      showCursor: false,
    };
    const typed = new Typed('#typed', options);
  }
  render() {
    return <TypedText id="typed" />;
  }
}

Typer.propTypes = {
  messages: PropTypes.arrayOf(PropTypes.string),
};

Typer.defaultProps = {
  messages: [],
};

export default Typer;
