import React from 'react';
import styled, { keyframes } from 'styled-components';

const Container = styled.div`
  padding-top: 20px;
`;

const blinker = keyframes`
  50% {
    opacity: 0;
  }
`;

const Title = styled.div`
  font-size: 20px;
  padding-top: 10px;
  padding-bottom: 10px;
`;

const InterestList = styled.div`
  display: flex;
  justify-content: space-between;
`;

const Interest = styled.div`
  display: inline-block;
  text-align: center;
`;

const Label = styled.div`
  font-size: 15px;
`;

const Icon = styled.i`
  color: ${props => props.theme.secondaryColor};
  &:hover{
    transform: scale(-1, 1);
    animation: ${blinker} 1s linear infinite;
  }
`;

const items = [
  {
    label: 'Gaming',
    className: 'fas fa-gamepad',
  }, {
    label: 'Football',
    className: 'far fa-futbol',
  }, {
    label: 'Traveling',
    className: 'fas fa-plane',
  }, {
    label: 'Coding',
    className: 'fas fa-code',
  },
];

function Interests() {
  return (
    <Container>
      <Title>Hobbies & Interests</Title>
      <InterestList>
        {items.map(i => (
          <Interest key={i.label}>
            <Icon className={i.className} />
            <Label>{i.label}</Label>
          </Interest>
          ))}
      </InterestList>
    </Container>
  );
}

export default Interests;
