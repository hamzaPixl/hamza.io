import React, { Component } from 'react';
import styled, { ThemeProvider } from 'styled-components';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Notifications from 'react-notify-toast';
import { dark } from '../theme';

import Home from './Home';
import Contact from './Contact';
import About from './About';
import Resume from './Resume';
import Projects from './Projects';
import NavigationBar from './NavigationBar';
import Footer from './Footer';

const Container = styled.div`
  background-color: ${props => props.theme.bodyColor};
  height: 100%;
  overflow: scroll;
`;

const views = {
  home: <Home />,
  about: <About />,
  resume: <Resume />,
  projects: <Projects />,
  contact: <Contact />,
};

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      theme: dark,
      view: <Home />,
    };
    this.changeView = this.changeView.bind(this);
  }

  changeView(name) {
    if (views[name]) {
      this.setState({ view: views[name] });
    }
  }

  render() {
    return (
      <ThemeProvider theme={this.state.theme}>
        <MuiThemeProvider>
          <Container>
            <Notifications />
            <NavigationBar handleChange={this.changeView} />
            {this.state.view}
            <Footer />
          </Container>
        </MuiThemeProvider>
      </ThemeProvider>
    );
  }
}

export default App;
