import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
`;

const Title = styled.div`
  font-size: 20px;
  padding-top: 15px;
  padding-bottom: 15px;
`;

const List = styled.div`
  border-left: 4px solid ${props => props.theme.secondaryColor};
  border-radius: 2px;
  padding-left: 10px;
  margin-left: 4px;
`;

const Item = styled.div`
  font-size: 15px;
  padding-bottom: 5px;
`;

const ItemTitle = styled.div`
`;

const ItemPlace = styled.div`
  opacity: 0.7;
`;

const ItemPeriod = styled.div`
  color: ${props => props.theme.secondaryColor};
`;

const items = [
  {
    title: 'Diploma (C.E.S.S)',
    place: 'Athénée Emile Bockstael',
    period: '2008 - 2014',
  },
  {
    title: 'Bachelor degree in management informatics',
    place: 'Institut Paul Lambin, Haute Ecole Léonard de Vinci',
    period: '2014 - 2017',
  },
];

function Education() {
  return (
    <Container>
      <Title>Education</Title>
      <List>
        {items.map(i => (
          <Item key={i.place}>
            <ItemPeriod>{i.period}</ItemPeriod>
            <ItemTitle>{i.title}</ItemTitle>
            <ItemPlace>{i.place}</ItemPlace>
          </Item>
          ))}
      </List>
    </Container>
  );
}

export default Education;
