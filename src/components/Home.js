import React from 'react';
import styled from 'styled-components';
import Typer from './Typer';
import avatar from '../assets/avatar.jpg';

const Container = styled.div`
  padding: 10%;
`;

const TextContainer = styled.div`
  font-size: 30px;
  color: ${props => props.theme.textColor};
  font-family: ${props => props.theme.fontFamily}, sans-serif;
  line-height: ${props => props.theme.lineHeight}rem;
  letter-spacing: ${props => props.theme.letterSpacing}rem;
`;

const Header = styled.div`
`;

const Name = styled.div`
  padding-top: 1%;
`;

const Description = styled.div`
  font-size: 15px;
  padding-top: 2%;
`;

const Avatar = styled.img`
  float: left;
  width: 40%;
  padding-right: 5%;
  transition: transform .2s;
  &:hover {
    transform: scale(1.1);
  }
`;

const jobs = ['Software <span>Engineer .</span>']
  .concat(['Fullstack', 'JavaScript', 'Frontend', 'Backend']
    .map(j => j.concat(' <span>Devlopper .</span>')));

function Home() {
  return (
    <Container>
      <Avatar src={avatar} alt="Avatar" />
      <TextContainer>
        <Header>Hello,</Header>
        <Name>I Am Mounir Hamza</Name>
        <Typer messages={jobs} />
        <Description>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit,
            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
            Ut enim ad minim veniam, quis nostrud exercitation ullamco
            laboris nisi ut aliquip ex ea commodo consequat.
            Duis aute irure dolor in reprehenderit in voluptate velit
            esse cillum dolore eu fugiat nulla pariatur.
        </Description>
      </TextContainer>
    </Container>
  );
}

export default Home;
