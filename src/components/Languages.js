import React from 'react';
import styled from 'styled-components';
import ProgressBar from './ProgressBar';

const Container = styled.div`
`;

const Title = styled.div`
  font-size: 20px;
  padding-top: 10px;
  padding-bottom: 10px;
`;

const LanguageList = styled.div`
  display: flex;
  flex-direction: column;
`;

const Language = styled.div`
  display: inline-block;
  padding-bottom: 10px;
`;

const Label = styled.div`
  padding-bottom: 2px;
  font-size: 14px;
  > span {
    color: ${props => props.theme.secondaryColor};
  }
`;

const items = [
  {
    label: 'French',
    percent: 100,
  }, {
    label: 'English',
    percent: 80,
  }, {
    label: 'Arabic',
    percent: 75,
  }, {
    label: 'Dutch',
    percent: 50,
  },
];

function Languages() {
  return (
    <Container>
      <Title>Languages</Title>
      <LanguageList>
        {items.map(i => (
          <Language key={i.label}>
            <Label>{i.label} <span>{i.percent} %</span></Label>
            <ProgressBar percent={i.percent} />
          </Language>
          ))}
      </LanguageList>
    </Container>
  );
}

export default Languages;
