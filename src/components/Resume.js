import React from 'react';
import styled from 'styled-components';

import Skills from './Skills';
import Languages from './Languages';
import Interests from './Interests';
import Education from './Education';
import Experience from './Experience';

const Container = styled.div`
  padding: 10%;
  font-size: 30px;
  color: ${props => props.theme.textColor};
  font-family: ${props => props.theme.fontFamily}, sans-serif;
  line-height: ${props => props.theme.lineHeight}rem;
  letter-spacing: ${props => props.theme.letterSpacing}rem;
`;

const Title = styled.div`
  color: ${props => props.theme.secondaryColor};
`;

const Row = styled.div`
  padding-top: 20px;
  display: flex;
  justify-content: space-between;
`;

function Resume() {
  return (
    <Container>
      <Title>Resume</Title>
      <Row>
        <Skills />
        <Languages />
      </Row>
      <Interests />
      <Row>
        <Experience />
        <Education />
      </Row>
    </Container>
  );
}

export default Resume;
