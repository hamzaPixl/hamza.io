import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
  flex: 1;
`;

const Title = styled.div`
  font-size: 20px;
  padding-top: 15px;
  padding-bottom: 15px;
`;

const List = styled.div`
  border-left: 4px solid ${props => props.theme.secondaryColor};
  border-radius: 2px;
  padding-left: 10px;
  margin-left: 4px;
`;

const Item = styled.div`
  font-size: 15px;
  padding-bottom: 5px;
`;

const ItemTitle = styled.div`
`;

const ItemPlace = styled.div`
  opacity: 0.7;
`;

const ItemDescription = styled.div`
  opacity: 0.7;
`;

const ItemPeriod = styled.div`
  color: ${props => props.theme.secondaryColor};
`;

const items = [
  {
    title: 'Web Devlopper Internship',
    place: 'Emakina - Brussels',
    period: 'Feb 2017 - Jun 2017',
    description: 'I create a desktop application that runs on a RaspBerryPi connected to TVs. It is used to show multiple widgets thanks to a RaspBerryPi connected to a TV like portfolio, weather forecast, real time of public transport ...',
  }, {
    title: 'Junior Software Engineer Full Stak',
    place: 'Qover - Brussels',
    period: 'Jul 2017 - Present',
    description: 'First job in a startup. Working as a Fullstack Devlopper to create a digital insurance service. Be a part of the future.',
  },
];

function Experience() {
  return (
    <Container>
      <Title>Experience</Title>
      <List>
        {items.map(i => (
          <Item key={i.place}>
            <ItemPeriod>{i.period}</ItemPeriod>
            <ItemTitle>{i.title}</ItemTitle>
            <ItemPlace>{i.place}</ItemPlace>
            <ItemDescription>{i.description}</ItemDescription>
          </Item>
          ))}
      </List>
    </Container>
  );
}

export default Experience;
