/* eslint jsx-a11y/anchor-is-valid: 0 */
import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const Container = styled.div`
  padding: 10px;
  top: 0;
  position: sticky;
  display: flex;
  justify-content: space-between;
  color: ${props => props.theme.primaryColor};
  background-color: ${props => props.theme.navigationBarColor};
  font-family: ${props => props.theme.fontFamily}, sans-serif;
  line-height: ${props => props.theme.lineHeight}rem;
  letter-spacing: ${props => props.theme.letterSpacing}rem;
`;

const Link = styled.a`
  padding: 5px 15px;
  transition: transform .3s ease-out;
  &:hover {
    color: ${props => props.theme.secondaryColor};
    transform: translateX(-5px);
  }
`;

const Links = styled.div`
  cursor: pointer;
`;

const Brand = styled.a`
  padding-left: 15px;
  cursor: pointer;
  color: ${props => props.theme.secondaryColor};
`;

const items = [
  {
    name: 'about',
    label: 'About Me',
  },
  {
    name: 'resume',
    label: 'Resume',
  },
  {
    name: 'projects',
    label: 'Projects',
  },
  {
    name: 'contact',
    label: 'Contact',
  },
];

function NavigationBar(props) {
  return (
    <Container>
      <Brand onClick={() => props.handleChange('home')}>
          HIO
      </Brand>
      <Links>
        {items.map(l => (
          <Link key={l.name} onClick={() => props.handleChange(l.name)}>
            {l.label}
          </Link>
          ))}
      </Links>
    </Container>
  );
}

NavigationBar.propTypes = {
  handleChange: PropTypes.func.isRequired,
};

export default NavigationBar;
