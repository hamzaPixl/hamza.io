import React from 'react';
import PropTypes from 'prop-types';
import LinearProgress from 'material-ui/LinearProgress';
import styled from 'styled-components';

import { basic } from '../theme';

export const Bar = styled(LinearProgress)`
  > div {
    border-radius: 4px;
  }
  position: static !important;
`;

class ProgressBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      completed: 0,
    };
  }

  componentDidMount() {
    this.timer = setTimeout(() => this.progress(5), 500);
  }

  componentWillUnmount() {
    clearTimeout(this.timer);
  }

  progress(completed) {
    if (completed > this.props.percent) {
      this.setState({ completed: this.props.percent });
    } else {
      this.setState({ completed });
      const diff = Math.random() * 10;
      this.timer = setTimeout(() => this.progress(completed + diff), 200);
    }
  }

  render() {
    const barStyle = {
      backgroundColor: 'transparent',
      width: '400px',
      height: '7px',
      borderRadius: '4px',
    };
    return (
      <Bar
        color={basic.secondaryColor}
        style={barStyle}
        mode="determinate"
        value={this.state.completed}
      />
    );
  }
}

ProgressBar.propTypes = {
  percent: PropTypes.number,
};

ProgressBar.defaultProps = {
  percent: 0,
};

export default ProgressBar;
