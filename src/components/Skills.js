import React from 'react';
import styled from 'styled-components';
import ProgressBar from './ProgressBar';

const Container = styled.div`
`;

const Title = styled.div`
  font-size: 20px;
  padding-top: 10px;
  padding-bottom: 10px;
`;

const SkillList = styled.div`
  display: flex;
  flex-direction: column;
`;

const Skill = styled.div`
  display: inline-block;
  padding-bottom: 10px;
`;

const Label = styled.div`
  padding-bottom: 2px;
  font-size: 14px;
  > span {
    color: ${props => props.theme.secondaryColor};
  }
`;

const items = [
  {
    label: 'Node.js',
    percent: 90,
  },
  {
    label: 'HTML / CSS',
    percent: 80,
  },
  {
    label: 'MongoDB / MySQL',
    percent: 75,
  },
  {
    label: 'Version Control / Git',
    percent: 60,
  }, {
    label: 'Docker / AWS',
    percent: 50,
  },
];

function Skills () {
  return (
    <Container>
      <Title>Skills & Abilities</Title>
      <SkillList>
        {items.map(i => (
          <Skill key={i.label}>
            <Label>{i.label}  <span>{i.percent} %</span></Label>
            <ProgressBar percent={i.percent} />
          </Skill>
          ))}
      </SkillList>
    </Container>
  );
}

export default Skills;
