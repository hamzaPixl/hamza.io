import React from 'react';
import { TextField } from 'material-ui';
import { notify } from 'react-notify-toast';
import styled from 'styled-components';
import { basic } from '../theme';

const Container = styled.div`
  padding: 10%;
  font-size: 30px;
  color: ${props => props.theme.textColor};
  font-family: ${props => props.theme.fontFamily}, sans-serif;
  line-height: ${props => props.theme.lineHeight}rem;
  letter-spacing: ${props => props.theme.letterSpacing}rem;
`;

const Title = styled.div`
  color: ${props => props.theme.secondaryColor};
`;

const SencondTitle = styled.div`
  padding: 2% 0 2% 0;
 > span {
    color: ${props => props.theme.secondaryColor};
  }
`;

const Form = styled.form`
  display: flex;
  flex-direction: column;
`;

const Input = styled(TextField)`
  > div, input {
    color: ${props => props.theme.textColor} !important;
  }
  margin-right: 3%;
`;

const Row = styled.div`
  display: inline-flex;
  padding-bottom: 4%;
`;

const Submit = styled.button`
  border-radius: 1px;
  width: 20%;
  color: ${props => props.theme.buttonTextColor};
  background: ${props => props.theme.buttonBackgroundColor};
  border: solid ${props => props.theme.buttonBorderColor} 2px;
  padding: 10px 20px 10px 20px;
  text-decoration: none;
  cursor: pointer;
  &:hover {
    color: ${props => props.theme.buttonHoverTextColor};
    background: ${props => props.theme.buttonHoverBackgroundColor};
    border: solid ${props => props.theme.buttonHoverBorderColor} 2px;
  }
`;

const Alternative = styled.div`
  padding-top: 4%;
  font-size: 15px;
  > span {
    color: ${props => props.theme.secondaryColor};
  }
`;

class Contact extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      myEmail: 'hmounir.work@gmail.com',
    };
    this.colorBorder = { borderColor: basic.secondaryColor };
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handleMessageChange = this.handleMessageChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleNameChange(e) {
    this.setState({ name: e.target.value });
  }

  handleEmailChange(e) {
    this.setState({ email: e.target.value });
  }

  handleMessageChange(e) {
    this.setState({ message: e.target.value });
  }

  handleSubmit() {
    if (this.state.name && this.state.email && this.state.message) {
      const carriageReturn = '%0D%0A %0D%0A';
      const emailBody = `Hello, I'm ${this.state.name}.${carriageReturn} ${this.state.message}.${carriageReturn} Please contact me at ${this.state.email}.`;
      window.location = `mailto:${this.state.myEmail}?subject=Contact from ${this.state.name}&body=${emailBody}`;
    } else {
      notify.show('You should fill every field.', 'error', 2000);
    }
  }

  render() {
    return (
      <Container>
        <Title>Contact</Title>
        <SencondTitle>I'd love to hear from you <span>{this.state.name}</span>.</SencondTitle>
        <Form>
          <Row>
            <Input underlineFocusStyle={this.colorBorder} fullWidth type="text" hintText="Name" name="name" onChange={this.handleNameChange} />
            <Input underlineFocusStyle={this.colorBorder} fullWidth type="email" hintText="Email" name="email" onChange={this.handleEmailChange} />
          </Row>
          <Row>
            <Input underlineFocusStyle={this.colorBorder} fullWidth type="text" hintText="Message" name="message" onChange={this.handleMessageChange} />
          </Row>
        </Form>
        <Submit onClick={() => this.handleSubmit()}>SEND MESSAGE</Submit>
        <Alternative>
          Or simply email me at <span>{this.state.myEmail}</span>
        </Alternative>
      </Container>
    );
  }
}

export default Contact;
