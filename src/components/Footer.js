import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
  bottom: 0;
  text-align: center;
  padding-top: 10%;
  font-size: 30px;
  color: ${props => props.theme.textColor};
  font-family: ${props => props.theme.fontFamily}, sans-serif;
  line-height: ${props => props.theme.lineHeight}rem;
  letter-spacing: ${props => props.theme.letterSpacing}rem;
`;

const Item = styled.li`
  padding-left: 20px;
`;

const Link = styled.a`
  font-size: 20px;
  color: ${props => props.theme.textColor};
  text-decoration: none;
  position: relative;
	box-sizing: border-box;
  &:hover {
    color: ${props => props.theme.secondaryColor};
  }
`;

const List = styled.ul`
  list-style-type: none;
  padding: 0;
  display: inline-flex;
`;

const items = [
  {
    link: 'https://www.facebook.com/profile.php?id=1104077828',
    className: 'fa fa-facebook',
    name: 'facebook',
  },
  {
    link: 'https://twitter.com/pixlhamza',
    className: 'fa fa-twitter',
    name: 'twitter',
  },
  {
    link: 'https://github.com/hamzaPixl/',
    className: 'fa fa-github',
    name: 'github',
  },
  {
    link: 'https://www.linkedin.com/in/hamza-mounir-0a7bb6139/',
    className: 'fa fa-linkedin',
    name: 'linkedin',
  },
  {
    link: 'https://www.instagram.com/hamza.pixl/',
    className: 'fa fa-instagram',
    name: 'instagram',
  },
];

function Footer() {
  return (
    <Container>
      <List>
        {items.map(i => (
          <Item key={i.name}>
            <Link href={i.link} className={i.className} />
          </Item>
        ))}
      </List>
    </Container>
  );
}

export default Footer;
