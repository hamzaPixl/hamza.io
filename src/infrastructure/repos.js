function getRepositoryInformation(repository, username) {
  const GITHUB_URL = `https://api.github.com/repos/${username}/${repository}`;
  return fetch(GITHUB_URL, { method: 'GET' })
    .then(res => res.json())
    .then(res => ({
      forks: res.forks_count,
      whatchers: res.watchers_count,
      description: res.description,
      stars: res.stargazers_count,
    }))
    .catch(({ code: 'ERROR_FETCH', message: 'Error on retrive the repository.' }));
}

function getNumberOfBranches(repository, username) {
  const BRANCHES_NUMBER_URL = `https://api.github.com/repos/${username}/${repository}/branches`;
  return fetch(BRANCHES_NUMBER_URL, { method: 'GET' })
    .then(res => res.json())
    .then(res => ({ branches: res.length }))
    .catch(({ code: 'ERROR_FETCH', message: 'Error on retreve the repository.' }));
}

function getNumberOfContributors(repository, username) {
  const CONTRIBUTORS_NUMBER_URL = `https://api.github.com/repos/${username}/${repository}/contributors`;
  return fetch(CONTRIBUTORS_NUMBER_URL, { method: 'GET' })
    .then(res => res.json())
    .then(res => ({ contributors: res.length }))
    .catch(({ code: 'ERROR_FETCH', message: 'Error on retreve the repository.' }));
}

function getNumberOfCommits(repository, username) {
  const COMMITS_NUMBER_URL = `https://api.github.com/repos/${username}/${repository}/contributors`;
  return fetch(COMMITS_NUMBER_URL, { method: 'GET' })
    .then(res => res.json())
    .then(res => ({ commits: res[0].contributions }))
    .catch(({ code: 'ERROR_FETCH', message: 'Error on retreve the repository.' }));
}

async function getAllInformation(repository, username) {
  const basicInformation = await getRepositoryInformation(repository, username);
  const numberBranches = await getNumberOfBranches(repository, username);
  const numberOfCommits = await getNumberOfCommits(repository, username);
  const numberOfContributors = await getNumberOfContributors(repository, username);

  return {
    username,
    repository,
    description: basicInformation.description,
    forks: basicInformation.forks,
    whatchers: basicInformation.whatchers,
    stars: basicInformation.stars,
    commits: numberOfCommits.commits,
    branches: numberBranches.branches,
    contributors: numberOfContributors.contributors,
  };
}

export default getAllInformation;
