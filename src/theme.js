const basic = {
  fontFamily: 'Roboto',
  fontWeight: 300,
  lineHeight: 2,
  letterSpacing: 0.01,
  titleSize: 40,
  secondaryColor: '#EF0D33',
};

const dark = {
  name: 'dark',
  bodyColor: '#000000',
  primaryColor: '#000000',
  textColor: '#FFFFFF',
  navigationBarColor: '#FFFFFF',

  buttonBorderColor: '#FFFFFF',
  buttonBackgroundColor: '#FFFFFF',
  buttonTextColor: '#000000',

  buttonHoverBorderColor: '#EF0D33',
  buttonHoverBackgroundColor: '#EF0D33',
  buttonHoverTextColor: '#FFFFFF',

  ...basic,
};

const light = {
  ...basic,
};

export {
  light,
  dark,
  basic,
};
